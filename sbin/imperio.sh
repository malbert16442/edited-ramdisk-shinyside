#!/system/bin/sh
export PATH=/sbin:/system/sbin:/system/bin:/system/xbin
busybox mount -o remount,rw /
busybox mount -o remount,rw /system /system
sync
exec /su/bin/daemonsu --auto-daemon
busybox rm -rf /system/app/BBCAgent
busybox rm -rf /system/app/Bridge
busybox rm -rf /system/app/ContainerAgent
busybox rm -rf /system/app/ContainerEventsRelayManager
busybox rm -rf /system/app/DiagMonAgent
busybox rm -rf /system/app/ELMAgent
busybox rm -rf /system/app/HLC
busybox rm -rf /system/app/KLMSAgent
busybox rm -rf /system/app/Knox
busybox rm -rf /system/app/KNOX
busybox rm -rf /system/app/RCPComponents
busybox rm -rf /system/app/SecurityLogAgent
busybox rm -rf /system/app/SPDClient
busybox rm -rf /system/app/SyncmlDM
busybox rm -rf /system/app/UniversalMDMClient
busybox rm -rf /system/priv-app/BBCAgent
busybox rm -rf /system/priv-app/Bridge
busybox rm -rf /system/priv-app/ContainerAgent
busybox rm -rf /system/priv-app/ContainerEventsRelayManager
busybox rm -rf /system/priv-app/DiagMonAgent
busybox rm -rf /system/priv-app/ELMAgent
busybox rm -rf /system/priv-app/HLC
busybox rm -rf /system/priv-app/KLMSAgent
busybox rm -rf /system/priv-app/Knox
busybox rm -rf /system/priv-app/KNOX
busybox rm -rf /system/priv-app/RCPComponents
busybox rm -rf /system/priv-app/SecurityLogAgent
busybox rm -rf /system/priv-app/SPDClient
busybox rm -rf /system/priv-app/SyncmlDM
busybox rm -rf /system/priv-app/UniversalMDMClient
busybox rm -rf /system/container
busybox sed -i 's/ro.securestorage.support=true/ro.securestorage.support=false/g' /system/build.prop
busybox mount -o remount,rw /
busybox mount -o remount,rw /system /system
sleep 1;
